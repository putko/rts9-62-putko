# README #
This is Novosibirsk metro modelling project repository.

## WTF is this? ##

## Parameters ##
* 2 lines
    * (1) 8 stations
    * (2) 5 stations
* 26 trains
    * 4 cars in each train
    * a car can carry 150 passengers
* [Intervals](http://www.nsk-metro.ru/index.php?event=menu&pname=interval)
* [Track map](http://trackmap.ru/img/tm_novosibirsk.png)
* [Passenger traffic](https://ru.wikipedia.org/wiki/Новосибирский_метрополитен#Пассажиропоток_и_статистика)

## TODO ##
1. Compilation instructions
2. Testing instructions